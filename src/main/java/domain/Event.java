package domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.joda.time.DateTime;
import org.joda.time.Days;


public class Event {
	private Integer id;
    private String name;
    private String start;
    private String end;
    private String description;
    private Integer ticket_price;
    private String date_format = "dd.MM.yy HH:mm";
    
    public Event(){
    }
    
    public Event(Integer id, String name, Date start, Date end, String description, Integer ticket_price){
        this.id = id;
        this.name = name;
        this.start = new SimpleDateFormat(date_format).format(start);
        this.end = new SimpleDateFormat(date_format).format(end);
        this.description = description;
        this.ticket_price = ticket_price;
    }
    
    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public Date getDateStart() throws ParseException {
		return new SimpleDateFormat(date_format).parse(start);
	}
	public Date getDateEnd() throws ParseException {
		return new SimpleDateFormat(date_format).parse(end);
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getTicket_price() {
		return ticket_price;
	}
	public void setTicket_price(Integer ticket_price) {
		this.ticket_price = ticket_price;
	}
	
	public String getTimeDescription() throws ParseException {
		long diff = (getDateEnd().getTime() - getDateStart().getTime());
    	int houres = (int) TimeUnit.MILLISECONDS.toHours(diff)%24; 
    	int minutes = (int) (TimeUnit.MILLISECONDS.toMinutes(diff)%60);
		int days = Days.daysBetween(new DateTime(getDateStart()), new DateTime(getDateEnd())).getDays();
		String time = Integer.toString(houres) + " godz. " + Integer.toString(minutes) + " min.";
    	
		if (days==0){
    		return time;
    	}
    	else{
    		return Integer.toString(days) + " dni " + time;
    	} 
	}
}
