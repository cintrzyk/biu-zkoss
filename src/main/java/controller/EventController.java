package controller;

import java.text.ParseException;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.*;

import service.EventService;
import service.EventServiceImpl;

import domain.Event;

public class EventController extends SelectorComposer<Component> {
	private static final long serialVersionUID = 1L;
    
	private EventService eventService = new EventServiceImpl();
	private ListModel<Event> eventAllModel = new ListModelList<Event>(eventService.find5());

	@Wire
    private Textbox keywordBox;
    @Wire
    private Listbox eventListbox;    
    @Wire
    private Listbox eventAllbox;
    @Wire
    private Label nameLabel;
    @Wire
    private Label ticket_priceLabel;
    @Wire
    private Label descriptionLabel;
    @Wire
    private Label timeLabel;
    
    
	@Listen("onClick = #searchButton")
    public void search(){
		String keyword = keywordBox.getValue();
        List<Event> results = eventService.search(keyword);
        eventListbox.setModel(new ListModelList<Event>(results));
    }
	
	@Listen("onSelect = #eventListbox")
    public void showDetail() throws ParseException{
        Event selected = eventListbox.getSelectedItem().getValue();
    	setEventDescription(selected);
    }
	
	@Listen("onSelect = #eventAllbox")
    public void showDetail2() throws ParseException{
        Event selected = eventAllbox.getSelectedItem().getValue();
    	setEventDescription(selected);
    }
	
	private void setEventDescription(Event selected) throws ParseException{
        nameLabel.setValue(selected.getName());
        descriptionLabel.setValue("Opis: "+selected.getDescription());
        timeLabel.setValue("Wydarzenie będzie trwało "+selected.getTimeDescription()); 
        ticket_priceLabel.setValue("Cena bilteu: "+selected.getTicket_price().toString()+" zł");
	}

	public ListModel<Event> getEventAllModel() {
		return eventAllModel;
	}

}


