package service;

import java.util.List;
import domain.Event;

public interface EventService {

    public List<Event> findAll();
    
    public List<Event> find5();

    public List<Event> search(String keyword);
}
