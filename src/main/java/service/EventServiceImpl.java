package service;

import java.text.ParseException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import domain.Event;

public class EventServiceImpl implements EventService{

    private List<Event> EventList= new LinkedList<Event>();
    private static int id = 1;

    @SuppressWarnings("deprecation")
	public EventServiceImpl() {
        EventList.add(new Event(id++,
    		"Zebranie fanów Javy.",
    		new Date(2013,1,12,13,33),
    		new Date(2013,1,22,16,44),
    		"Będzie nudno i nie warto przychodzić ale zapraszamy!",
    		1
        ));
        EventList.add(new Event(id++, 
    		"Zebranie fanów Ruby.", 
    		new Date(2013,0,02,13,10), 
    		new Date(2013,0,30,16,30), 
    		"Przyjdzie do nas! Spotkanie fanów Javy będzie nudne jak flaki z olejem.", 
    		100
        ));
        EventList.add(new Event(id++, 
    		"Akademia nauki", 
    		new Date(2013,1,24,19,00), 
    		new Date(2013,3,30,16,30), 
    		"Odbęda się pokazy programowania na czas.", 
    		23
        ));
        EventList.add(new Event(id++, 
    		"Pływanie na powietrzu bez wody", 
    		new Date(2013,6,29,14,00), 
    		new Date(2013,7,30,16,20), 
    		"Zawody odbęda się bez nadzoru dorosłych. Wszystkich śmiałków serdecznie zapraszamy!", 
    		456
        ));
        EventList.add(new Event(id++, 
    		"Kurs nauki jazdy w taczce.", 
    		new Date(2013,2,12,10,00), 
    		new Date(2013,4,30,11,30), 
    		"Profesjonalna obsługa oraz gwaracja zadowolenia i dużej dawki adrenaliny.", 
    		56
        ));
        EventList.add(new Event(id++, 
    		"Pokaz żywych ogni", 
    		new Date(2013,1,06,12,00), 
    		new Date(2013,3,30,18,30), 
    		"Przyjdź a się przekonasz!!!", 
    		124
        ));
    }
    
    
    public List<Event> findAll(){
        return EventList;
    }
    
    public List<Event> find5(){
        Collections.sort(EventList, new Comparator<Event>() {
            public int compare(Event m1, Event m2) {
                try {
					return  m2.getDateStart().compareTo(m1.getDateStart());
				} catch (ParseException e) { e.printStackTrace(); }
				return 1;
            }
        });
        Collections.reverse(EventList);
        return EventList.subList(0, 5);
    }
    
    public List<Event> search(String keyword){
        List<Event> result = new LinkedList<Event>();
        if (keyword==null || "".equals(keyword)){
                result = EventList;
        }else{
            for (Event c: EventList){
                if (c.getName().toLowerCase().contains(keyword.toLowerCase())
                    || c.getDescription().toLowerCase().contains(keyword.toLowerCase())){
                        result.add(c);
                }
            }
        }
        return result;
    }
}
